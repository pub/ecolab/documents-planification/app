# Bienvenu à l'outil de visualisation des documents de planification


## Installation

Python et [pip](https://pip.pypa.io/en/stable/) sont nécessaires.

```shell
git clone https://gitlab-forge.din.developpement-durable.gouv.fr/pub/ecolab/docs-planification
pip install -r docs-planification/requirements.txt
```

## Running
```shell
cd docs-planification
streamlit run app/1-Accueil.py
```

Il faut avoir accès au données soit en demandant directement l'accès à la base postgresql hébergée sur le SSPCoud soit en les téléchargeant grâce aux projets [docs-planification-dbt](https://gitlab-forge.din.developpement-durable.gouv.fr/martin.huot/docs-panification-dbt) et [docs-planification-import](https://gitlab-forge.din.developpement-durable.gouv.fr/martin.huot/docs-planification-import)

## Creation et utilisation de conteneur

### Au SSPcloud

L'application est deployée avec `helm` en utilisant le package [docs-planification-deployment](https://gitlab-forge.din.developpement-durable.gouv.fr/martin.huot/docs-planification-deployment) et est accessible sur [https://docs-plan.lab.sspcloud.fr/](https://docs-plan.lab.sspcloud.fr/).


### En local

```shell
docker build -t docs-planification .
docker run --rm -p 3838:3838 docs-planification
```

L'application sera disponible sur [localhost:3838](localhost:3838).
