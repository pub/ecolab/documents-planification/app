# general.py

## create_conn

cré une connexion psycopg2

Returns:
    psycog2 conn: connexion psycopg2

## remove_page_items

Remove main menu and footer

Args:
    menu (bool, default=True): remove hamburger menu
    footer (bool, default=True): remove footer "Made with Streamlit"

Returns: None

## creation_engine

cré une connexion sqlalchemy

Returns:
    engine (slqalchemy engine): sql alchemy engine
    connexion_str(str) : connexion string

## select_type_zone

Cré une selectbox pour changer de type de zone

Returns:
    None: stocke le résultat dans st.session_state

## on_change_type_zone

fonction appelée lorsque l'on change de type de zone, cette fonction réinitialise la zone 
à sa valeur par défault

## liste_zones

Donne la liste des zones correspondant à un type de zone (ex : la liste des départements)

Args:
    type_zone (str):Un des éléments de la liste type_zone

Returns:
    pandas DataFrame: la liste des zones sous la forme 'nom_de_la_zone code_de_la_zone'

## select_zone

Permet de créer la selectbox pour choisir la zone

Returns:
    None: Le résulat est stocké dans st.session_state

## on_change_zone

Fonction appelée lorsqu'on change de zone pour la première fois depuis le changement de type zone, la fonction permet de passer
st.session_state['zone appelée']  à 1 (utile pour le fonctionnement de mode_territoire et mode_doc)

## select_maille

Crée la selectbox permettant de choisir le type de maille
Seules les mailles inférieures ou égales au type de zone sont proposée
le résultat est stocké dans st.session_state

## select_mode

Permet de choisir le mode (mode doc ou mode territopire) avec des boutons
Le résultat est stocké dans st.session_state

## infos

Donne les infos sur un territoires, ces infos sont celles qui seront affichées au dessus de la carte en mode territoire

Args:
    type_zone (str): Un des élements de type_zone
    zone (str, optional): Le code géographique de la zone sélectionnée. Defaults to None.

Returns:
    list: Chaque élement correspond à une géographie (niveau supérieur, niveau transverse...)

## infos_sup_inf

Récupère les informations à afficher dans la fiche territoire

Args:
    type_zone (str): Un des éléments de la liste type_zones
    zone (str): codegeo de la zone
    docs (list): liste des documents à prendre en compte

Returns:
    list: un élément par tableau de la fiche territoire

## select_docs

cré la selectbox pour sélectionner le type de document en mode document

Returns:
    None: le résultat est stocké dans st.session_state

## choix_du_doc

Crée la selectbox pour choisir un document particulier en mode doc, le résultat est stocké dans st.session_state

Args:
    type_doc (str): type de document
    type_zone (str): type de zone
    zone (str): codegeo de la zone géographique considérée

## infos_doc

Crée le petit texte que s'affiche sous la carte en mode doc (et donne plus d'informations sur un document)

Args:
    code_doc (str): code du codument
    type_doc (str): type de document

Returns:
    str: Renvoie le résultat sous forme de str du html à afficher

## checkbox_doc

cré une checkbox pour savoir si un type de document doit être affiché ou non dans la fiche territoire

Args:
    doc (str): type du doc

## choix_docs

Cré des checkbox pour savoir quels sont les documents à afficher en fiche territoire

Returns:
    list(str): liste des types de documents à afficher dans la fiche territoire

## page_existe

Fonction innutilisée actuellement

Args:
    url (str): url

Returns:
    bool: True si l'url existe, False sinon

# map_functions

## load_contours_2

  Récupère tous les polygones d'une zone donnée pour une maille donnée

  Args:
      maille (str): maille
      zone (str): codegeo de la zone
      type_zone (str): type de zone

  Returns:
      geoDataFrame: renvoie un dataframe avec les coordonnée et le codegeo de tous les polygones

## localisation_zoom

  renvoie la localisation d'une zone sur la carte

  Args:
      zone (str): codegeo de la zone
      type_zone (str): type de la zone

  Returns:
      list: liste latitude, longitude

## get_map_test_popup_2

  Crée la carte du mode territoire

  Args:
      maille (str): maille d'affichage
      zone (str): codegeo de la zone à afficher
      type_zone (str): type de la zone

  Returns:
      folium map: carte folium du mode territoire

## get_map_documents_3

  Crée la carte du mode doc

  Args:
      type_de_doc (str): type de document
      type_zone (str): type de zone
      zone (str): codegeo de la zone

  Returns:
      map (folium map): carte du mode doc
      liste_sous_etats (list): liste des sous états d'avancement présents sur la carte
      liste_etats (list): liste des sous états d'avancement présents sur la carte
