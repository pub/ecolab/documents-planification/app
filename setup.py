import os
from pathlib import Path

from setuptools import find_packages, setup

PKG_NAME = "app"
VERSION = os.getenv("BUILD_VERSION", "0.0.2")

with open("requirements.txt") as f:
    requirements = f.read().splitlines()

if __name__ == "__main__":

    print(f"Building wheel {PKG_NAME}-{VERSION}")

    # Dynamically set the __version__ attribute
    cwd = Path(__file__).parent.absolute()
    with open(cwd.joinpath(PKG_NAME, "version.py"), "w", encoding="utf-8") as f:
        f.write(f"__version__ = '{VERSION}'\n")

    setup(
        name=PKG_NAME,
        version=VERSION,
        author="Martin Huot",
        author_email="martin.huot@i-carre.net",
        description="Cartographie des documents de planification écologique du territoire",
        url="https://gitlab-forge.din.developpement-durable.gouv.fr/pub/ecolab/docs-planification",
        packages=find_packages(),
        classifiers=[
            "Programming Language :: Python",
            "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
            "Operating System :: OS Independent",
        ],
        python_requires=">=3.8",
        install_requires=requirements,
        include_package_data=True,
    )