FROM python:3.10

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

COPY ./requirements.txt requirements.txt
COPY ./app app
COPY ./setup.py setup.py

RUN pip install -r requirements.txt

RUN pip install --upgrade pip setuptools wheel \
    && pip install -e . \
    && pip cache purge \
    && rm -rf /root/.cache/pip

EXPOSE 8501

ENTRYPOINT ["streamlit", "run"]

CMD ["app/1-Accueil.py"]
