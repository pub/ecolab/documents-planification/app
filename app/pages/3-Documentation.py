import streamlit as st
from streamlit_extras.switch_page_button import switch_page

st.set_page_config("Documentation", layout='wide')
st.write('# Documentation')
for key in st.session_state.keys(): #si on ne fait pas ça il ya un bug quand on revient à l'app normale
    del st.session_state[key]
if st.button("Cliquez ici pour retourner à l'accueil"):
    switch_page('Accueil')
doc1 = '''
### Rôle de l’outil :

Cet outil a pour but de cartographier les documents de planification écologique  du territoire français. Les documents actuellement disponibles sont les PCAETs, les SCoTs, les SAGEs, les SDAGEs et les documents d’urbanisme (PLUs et PLUis) 

### Fonctionnement de l’outil :

Deux modes sont disponibles dans l’outil :

**1 : Le mode territoire**

Ce mode se lance soit par défaut soit en cliquant sur le bouton ‘recherche par territoire’.

Dans ce mode vous pouvez sélectionner une zone d’affichage (une région, un département, un EPCI…) ainsi que la maille des contours affichés sur la carte.

Lorsque la carte est affichée, il est possible :
- De cliquer sur les différents contours pour obtenir quelques informations à leur sujet (les documents qui s’affichent sont tous les documents à l’exception de ceux de niveau inférieur (voir ci-après)) \n
- De cliquer sur le bouton ‘Cliquez ici pour voir la fiche du territoire’. Dans ce cas, 4 boutons s’affichent pour obtenir des informations plus précises sur la zone sélectionnée : \n
    **Documents de niveau supérieur** : ce sont les documents s’appliquant sur un territoire plus vaste que la zone sélectionnée tout en englobant l’entièreté de cette zone. \n
    **Documents s’appliquant sur tout le territoire :** ce sont les documents ayant le même périmètre d’application que la zone sélectionnée. \n
    **Documents s’appliquant sur une partie du territoire :** ce sont les documents s‘appliquant en partie sur une partie sur la zone sélectionnée et en partie sur les territoires alentours. \n
    **Documents de niveau inférieur :** Il ne s'agit pas ici de lister les documents, mais d'avoir des statistiques sur quelle proportion du territoire est couverte par tel ou tel document.  \n

**2 : Le mode document**

Ce mode s’active en cliquant sur ‘Recherche par document’.

Un menu déroulant vous permet de choisir votre zone et un type de document.

Ensuite, une carte de la zone sélectionnée s’affiche avec les contours des documents qu’elle contient.
Les différents documents sont alors colorés en fonction de leur état d'avancement.
'''
doc2='''

### Sources
'''

sources ='''
<div>
  <b>1 : Les documents</b><br>
  <p style="margin-left: 20px;"><b>  - PCAET :</b> Ademe (<a href="https://www.territoires-climat.ademe.fr/opendata">ici</a> et <a href="https://www.territoires-climat.ademe.fr/ressource/646-232">ici (DGEC)</a>)<br>
  <b>  - Documents d'urbanisme :</b> <a href="https://www.geoportail-urbanisme.gouv.fr/services/">Géoportail de l'urbanisme</a> (flux wfs)<br>
  <b>  - SCoT :</b> <a href="https://www.geoportail-urbanisme.gouv.fr/services/">Géoportail de l'urbanisme</a> (flux wfs)<br>
  <b>  - SAGE :</b> <a href="https://www.gesteau.fr/telecharger/sage">GESTEAU</a><br>
  <b>  - SDAGE :</b> <a href="https://www.gesteau.fr/consulter-les-sdage">GESTEAU</a><br>
  <b>  - CRTE :</b> <a href="https://agence-cohesion-territoires.gouv.fr/crte-signes-840">ANCT</a><br>
  <b>  - SRADDET :</b> Documents récupérés sur les sites des diverses régions<br></p>
  <b>  - PDM :</b> <a href="https://plans-mobilite.cerema.fr/caracteristiques">Cerema</a><br>
</div>
<div>
<ul>
    <b>2 : Les référentiels géographiques</b><br>
    <p style="margin-left: 20px;">
        <b>- Les communes :</b> <a href="https://geoservices.ign.fr/services-geoplateforme-diffusion">IGN - Géoplateforme (Admin Express)</a><br>
        <b>- Les collectivités :</b><br>
            <b> &nbsp&nbsp&nbsp- EPCI à fiscalité propre :</b> <a href="https://www.collectivites-locales.gouv.fr/institutions/liste-et-composition-des-epci-fiscalite-propre">collectivites-locales.gouv.fr</a><br>
            <b> &nbsp&nbsp&nbsp- Autres collectivités :</b> <a href="https://www.banatic.interieur.gouv.fr/V5/fichiers-en-telechargement/fichiers-telech.php">BANATIC</a><br>
        <b>- Bassins des SDAGE :</b> <a href="https://www.sandre.eaufrance.fr/atlas/srv/fre/catalog.search#/metadata/e67df0d5-06ce-48c8-9eb8-2f35dbb84e43">Sandre</a><br>
    </p>
</ul>
</div>
'''

st.write(doc1)
st.write(doc2)
st.write(sources, unsafe_allow_html=True)
st.write("\n Contact : amal.yeferni@developpement-durable.gouv.fr ou [ce forms](https://docs.google.com/forms/d/e/1FAIpQLSffRpE7uF3RDVwKKmdLg7CfDa5UHwZjLHiCQq8YOi6uJUhJMQ/viewform?usp=sf_link)")
