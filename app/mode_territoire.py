# Import des modules
import general as g
import map_functions as mf
import streamlit as st
from streamlit_extras.switch_page_button import switch_page
import os
from fiche_territoire import page_fiche_territoire

def territoire():  # affichage du mode territoire

    # Menus déroulants
    st.sidebar.write("#### Sélection du territoire à étudier")
    g.select_type_zone()  # Menu déroulant pour choisir le type de zone
    g.select_zone()  # Menu déroulant pour choisir la zone

    st.sidebar.write("#### Sélection de la maille d'affichage")
    g.select_maille()  # Menu déroulant pour choisir la maile d'affichage

    # Boutons permettant de changer de page
    if st.button('Cliquez ici pour voir la documentation'):
        switch_page('Documentation')
    if st.button("Cliquez ici pour voir la fiche du territoire"):
        st.session_state['boutons']['fiche_territoire'] = True
        st.rerun()

    try:
        #print('zone_1', st.session_state['zone_1'], 'zone_2', st.session_state['zone_2'],'zone', st.session_state['zone'], 'type_zone', st.session_state['type_zone'], 'type_zone_temp', st.session_state['type_zone_temp'])
        if st.session_state['type_zone'] == 'France entière' or st.session_state['zone appelée'] == 1:
            # Affichage des infos sur la zone sélectionnée
            t = g.infos(st.session_state['type_zone'], st.session_state['zone'])  # Chargement des infos sur la zone
            txt1 = "<b>Documents de niveau supérieur :</b> "+t[0]  # Documents de niveau superieur
            txt2 = "<b>Documents de " + st.session_state['zone_2'] + " :</b> " + t[1]  # Documents de la zone sélectionnée
            txt3 = "<b>Documents s'appliquant s'appliquant sur une partie de " + st.session_state['zone_2'] + " :</b> " + t[2]  # Document transversaux
            txt4 = "<b>Documents de niveau inférieur :</b> " + t[3]  # Documents de niveau inférieur
            formatted_text = f"{txt1}<br>{txt2}<br>{txt3}<br>{txt4}"  # fusion des textes html
            st.markdown(formatted_text, unsafe_allow_html=True)  # affichage du contenu html


            # Affichage de la carte
            path = f"app/save_map/territoire_{st.session_state['maille']}_{st.session_state['zone']}_{st.session_state['type_zone']}.html"  # path où est / sera stocké la carte

            if os.path.isfile(path):  # Affiche la carte si elle existe déjà
                with open(path, 'r') as f:  # Lecture de la carte
                    html_data = f.read()
                st.components.v1.html(html_data, height=500, scrolling=True)  # Affichage de la carte dans streamlit
                st.download_button(  # Bouton de téléchargement de la carte
                    data=html_data,
                    label='Télécharger la carte',
                    file_name=f"carte_{st.session_state['maille']}_{st.session_state['zone']}_{st.session_state['type_zone']}.html"
                )
            else:  # Création de la carte si elle n'existe pas
                map = mf.get_map_test_popup_2(st.session_state['maille'], st.session_state['zone'], st.session_state['type_zone'])  # création de la carte
                map.save(path)
                with open(path, 'r') as f:  # lecture de la carte nouvellement créée
                    html_data = f.read()
                st.components.v1.html(html_data, height=500, scrolling=True)  # affichage de la carte dans streamlit
                st.download_button(  # Bouton de téléchargement de la carte
                    data=html_data,
                    label='Télécharger la carte',
                    file_name=f"carte_{st.session_state['maille']}_{st.session_state['zone']}_{st.session_state['type_zone']}.html"
                )
    except:
        st.error('''Une erreur a eu lieu, veuillez réessayer votre action.
        Si l'erreur persiste, merci de la signaler sur ce [formulaire](https://docs.google.com/forms/d/e/1FAIpQLSffRpE7uF3RDVwKKmdLg7CfDa5UHwZjLHiCQq8YOi6uJUhJMQ/viewform?usp=sf_link)
        ''')
