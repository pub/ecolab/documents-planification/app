import streamlit as st
import pandas as pd
from general import infos_sup_inf
import general as g
from streamlit_extras.switch_page_button import switch_page

def page_fiche_territoire():
    # Menus déroulants
    st.sidebar.write("#### Sélection du territoire à étudier")
    g.select_type_zone()  # Menu déroulant pour choisir le type de zone
    g.select_zone()  # Menu déroulant pour choisir la zone

    if st.session_state['type_zone'] == 'France entière' or st.session_state['zone appelée'] == 1:
        st.write('## Documents de ' + st.session_state['zone_2'] + ' :')
    if st.button(":blue[Retourner à l'accueil]"):
        st.session_state['boutons']['fiche_territoire'] = False
        st.rerun()
    for doc in g.types_docs:
        g.checkbox_doc(doc)
    docs = g.choix_docs()

    if st.session_state['type_zone'] == 'France entière' or st.session_state['zone appelée'] == 1:
        if 'zone' not in st.session_state:
            data = infos_sup_inf(st.session_state['type_zone'], docs=docs)
        else:
            data = infos_sup_inf(st.session_state['type_zone'], st.session_state['zone'], docs)

        docs = g.choix_docs()
        # Création des boutons pour contrôler l'affichage
        show_table1 = st.button('Documents de niveau supérieur')
        show_table2 = st.button("Documents s'appliquant sur tout le territoire")
        show_table3 = st.button("Documents s'appliquants sur une partie du territoire")
        show_table4 = st.button("Statistiques sur le territoire")

        # Affichage des tableaux en fonction des boutons cliqués
        if show_table1:
            st.write('<b>Documents de niveau supérieur</b>', unsafe_allow_html= True)
            st.components.v1.html(data[0], height=810)

        if show_table2:
            st.write('<b>Documents du territoire</b>', unsafe_allow_html=True)
            st.components.v1.html(data[1], height=810, scrolling=True)

        if show_table3:
            st.write("<b>Documents s'appliquants sur une partie du territoire</b>", unsafe_allow_html=True)
            st.components.v1.html(data[2], height=810)

        if show_table4:
            st.write("<b>Documents de niveau inférieur</b>", unsafe_allow_html=True)
            st.components.v1.html(data[3], height=810,scrolling=True)
