__doc__ = """Fonctions, elements (barre laterale, données, sélections) et définitions
globales pour l'application"""

# Import des modules, general ne doit faire appel à aucun autre fuichier du package "app"
import os
import pandas as pd
import streamlit as st
import psycopg2
from sqlalchemy import create_engine
import numpy as np
import requests
from time import sleep
import logging

type_zones = ['France entière', 'Région', 'Département', 'EPCI', 'Commune']  # Différentes mailles de l'application
type_zones_base = {'France entière': 'France', 'Région': 'reg', 'Département':'dep', 'EPCI': 'epci', 'Commune': 'commune'}  # Correspondance entre les mailles de l'app et les mailles de la base postgre
# TODO : remplacer types_docs par une requête vers la base postgre
types_docs = ['CRTE', 'SCoT', 'SAGE', 'SDAGE', 'PCAET', 'PLU', 'PLUI', 'CC', 'PDM', 'SRADDET']  # Les différents documents intégrés
correspondance_noms_docs = {  # Correspondance entre les colonnes dans le schéma de données et dans l'appli
    'code_doc': 'Identifiant',
    'nom_doc': 'Nom du document',
    'structure_porteuse': 'Structure porteuse',
    'etat_document': "Etat d'avancement",
    'sous_etat': "Sous état d'avancement / autre caractéristique",
    "type_evt": "Dernier évènement",
    "date_evt": 'Date du dernier évènement',
    "date_creation": "Date de création du document",
    "type_doc": 'Type de document',
    "option_commentaires": 'Commentaire',
    'superficie': "Superficie",
    'pop': "Population"
}

etats_valides = ('Opposable', 'Signé', 'Adopté', 'Approuvé')
# Connexion à la base de données
# Toute connexion ouverte doit être fermée après être utilisé : une fonction qui fait appel à create_conn doit forcément comrendre la ligne conn.close()
def create_conn():
    """cré une connexion psycopg2

    Returns:
        psycog2 conn: connexion psycopg2
    """    
    conn = psycopg2.connect(
        host=os.environ.get('POSTGRESQL_HOST'),
        port=os.environ.get('POSTGRESQL_PORT'),
        dbname=os.environ.get('POSTGRESQL_DB'),
        user=os.environ.get('POSTGRESQL_USER'),
        password=os.environ.get('POSTGRESQL_PASSWORD')
    )
    return conn


def remove_page_items(menu=True, footer=True):
    """
    Remove main menu and footer

    Args:
        menu (bool, default=True): remove hamburger menu
        footer (bool, default=True): remove footer "Made with Streamlit"

    Returns: None
    """
    if menu:
        st.markdown(
            """ <style> #MainMenu {visibility: hidden;} </style> """,
            unsafe_allow_html=True,
        )
    if footer:
        st.markdown(
            """ <style> footer {visibility: hidden;} </style> """,
            unsafe_allow_html=True,
        )


@st.cache_resource
def creation_engine():
    """cré une connexion sqlalchemy

    Returns:
        engine (slqalchemy engine): sql alchemy engine
        connexion_str(str) : connexion string
    """    
    # Crée le lien de connexion à la base postgresql
    db_host = os.environ.get('POSTGRESQL_HOST')
    db_port = os.environ.get('POSTGRESQL_PORT')
    db_name = os.environ.get('POSTGRESQL_DB')
    db_user = os.environ.get('POSTGRESQL_USER')
    db_password = os.environ.get('POSTGRESQL_PASSWORD')

    # Créer la chaîne de connexion à la base de données
    connexion_str = f'postgresql://{db_user}:{db_password}@{db_host}:{db_port}/{db_name}'

    # Établir une connexion à la base de données
    engine = create_engine(connexion_str)
    return engine, connexion_str


engine, connexion_str = creation_engine()


# TODO : trier par ordre alphabétique
def select_type_zone():
    """Cré une selectbox pour changer de type de zone

    Returns:
        None: stocke le résultat dans st.session_state
    """    
    # Menu déroulant permettant de choisir le type de zone
    # Le choix de l'utilisateur est stocké dans st.session_state
    # permet de renvoyer par défault l'ancien choix de l'utilisateur si type_zone est déjà dans st.session_state
    # le fait d'utiliser vraie_type_zone qui dépend de 'type_zone_temp' avant même que type_zone_temp ai été modifié est vraiment bizarre et ce n'est pas propre
    # Néanmoins ça marche alors que si je fais autrement, il faut sélectionner deux fois le type de zone pour que ça marche on laisse donc ce drôle de mécanisme pour le moment
    if 'type_zone' not in st.session_state:
        type_zone = st.sidebar.selectbox("Choisissez le type de territoire :", type_zones)
        st.session_state['type_zone'] = type_zone
        st.session_state['type_zone_temp'] = type_zone
    else:  # permet de renvoyer par défault l'ancien choix de l'utilisateur si type_zone est déjà dans st.session_state
        type_zones_2 = pd.DataFrame(type_zones)
        vraie_type_zone = st.session_state.get('type_zone_temp')
        def index(df, valeur):
            return df.index[df[0] == valeur].tolist()[0]
        index = index(type_zones_2, st.session_state.get('type_zone_temp'))
        st.sidebar.selectbox("Choisissez le type de territoire :", type_zones_2, index, on_change=on_change_type_zone, key='type_zone_temp')
        st.session_state['type_zone'] = vraie_type_zone


def on_change_type_zone():
    """fonction appelée lorsque l'on change de type de zone, cette fonction réinitialise la zone 
    à sa valeur par défault
    """    
    st.session_state['zone'] = None
    st.session_state['zone_2'] = None
    st.session_state['zone appelée'] = 0
    print('on change appelée')
    # st.session_state['i'] = 0


def liste_zones(type_zone):
    """Donne la liste des zones correspondant à un type de zone (ex : la liste des départements)

    Args:
        type_zone (str):Un des éléments de la liste type_zone

    Returns:
        pandas DataFrame: la liste des zones sous la forme 'nom_de_la_zone code_de_la_zone'
    """    
    # Fonction permettant d'obtenir toutes les zones correspondant à un type de zone
    requete = f'''
    SELECT CONCAT(codegeo,' ',nom) as fusion 
    FROM {os.environ.get('POSTGRE_SCHEMA')}.liste_ref
    WHERE type_geo = {"'" + type_zones_base[type_zone] + "';"}
    '''
    return pd.read_sql(requete, engine)


# TODO : trier par ordre alphabétique
def select_zone():
    """Permet de créer la selectbox pour choisir la zone

    Returns:
        None: Le résulat est stocké dans st.session_state
    """    
    # Création du menu déroulant pour choisir la zone
    # le résulat est stocké dans st.session_state
    # zone correspond uniquement au codegeo
    # zone_2 correspond au codegeo + libgeo (ce que voit l'utilisateur)
    # Actuellement, il faut cliquer deux fois la deuxième fois qu'on change de zone après avoir changé de type de zone
    if st.session_state['type_zone'] == 'France entière':
        st.session_state['zone'] = 'None'
        st.session_state['zone_2'] = 'France'
    else:
        if 'zone' not in st.session_state or st.session_state['zone'] is None or st.session_state['zone'] == 'None':
            noms = liste_zones(st.session_state['type_zone'])
            st.sidebar.selectbox('Choisissez le territoire :', noms, on_change=on_change_zone, index=None, key='zone_1')
            if st.session_state.zone_1 is not None:
                st.session_state['zone'] = st.session_state.get('zone_1').split(" ")[0]
            st.session_state['zone_2'] = st.session_state.get('zone_1')
            print(st.session_state['zone_2'])
        else:  # permet de renvoyer par défault l'ancien choix de l'utilisateur si type_zone est déjà dans st.session_state
            # le fait d'utiliser zone_2_originale qui dépend de 'zone_1' avant même que zone_1 ai été modifié est vraiment bizarre et ce n'est pas propre
            # Néanmoins ça marche alors que si je fais autrement, il faut sélectionner deux fois la zone pour que ça marche on laisse donc ce drôle de mécanisme pour le moment
            noms = liste_zones(st.session_state['type_zone'])
            zone_2_originale = st.session_state.get('zone_1')
            '''
            if 'i' not in st.session_state:
                st.session_state['i'] = 0
            else:
                st.session_state['i'] = st.session_state['i'] + 1
            print('numero_essai', st.session_state['i'])
            print('avant fonction')
            print('zone_2_originale', zone_2_originale)
            print('zone_2', st.session_state.get('zone_2'))
            print('zone', st.session_state.get('zone'))
            '''
            def index(df, valeur):
                try:
                    return df.index[df['fusion'] == valeur].tolist()[0]
                except:
                    pass

            index = index(noms, st.session_state.get('zone_1'))
            st.sidebar.selectbox('Choisissez le territoire :', noms, index, key='zone_1')
            st.session_state['zone_2'] = zone_2_originale
            if st.session_state['zone_1'] is not None:
                st.session_state['zone'] = zone_2_originale.split(" ")[0]
            #print('après fonction')
            #print('zone_1', st.session_state.get('zone_1'))
            #print('zone_2', st.session_state.get('zone_2'))
            #print('zone', st.session_state.get('zone'))

def on_change_zone():
    """Fonction appelée lorsqu'on change de zone pour la première fois depuis le changement de type zone, la fonction permet de passer
    st.session_state['zone appelée']  à 1 (utile pour le fonctionnement de mode_territoire et mode_doc)
    """    
    st.session_state['zone appelée'] = 1


# TODO : trier par ordre alphabétique
def select_maille():
    """Crée la selectbox permettant de choisir le type de maille
    Seules les mailles inférieures ou égales au type de zone sont proposée
    le résultat est stocké dans st.session_state
    """    
    # idem select_zone mais pour la maille
    # Seule des mailles inférieures ou égales au type de zone sont possible
    i = 0
    for x in type_zones:
        if x == st.session_state['type_zone']:
            if 'France entière' == st.session_state['type_zone']:  # empêche de choisir la maille epci, ou commune si la zone est la France en entier
                st.session_state['maille'] = st.sidebar.selectbox('Choisissez la maille',type_zones[max(1,i):-2])
            else:
                st.session_state['maille'] = st.sidebar.selectbox('Choisissez la maille',type_zones[max(1,i):])
        i += 1


def select_mode():
    """Permet de choisir le mode (mode doc ou mode territopire) avec des boutons
    Le résultat est stocké dans st.session_state
    """    
    # Permet de choisir entre le mode doc et le mode territoire
    # Le résultat est stocké dans st.session_state
    if 'boutons' not in st.session_state:
        st.session_state['boutons'] = {'territoire' : True, 'doc' : False}
    # Si le Bouton 1 est cliqué, désactiver le Bouton 2
    if st.sidebar.button("Recherche par territoire"):
        st.session_state['boutons']['territoire'] = True
        st.session_state['boutons']['doc'] = False

    # Si le Bouton 2 est cliqué, désactiver le Bouton 1
    if st.sidebar.button("Recherche par type de document"):
        st.session_state['boutons']['territoire'] = False
        st.session_state['boutons']['doc'] = True


# TODO : donner les infos de part de la population / superficie couverte
def infos(type_zone, zone=None):
    """Donne les infos sur un territoires, ces infos sont celles qui seront affichées au dessus de la carte en mode territoire

    Args:
        type_zone (str): Un des élements de type_zone
        zone (str, optional): Le code géographique de la zone sélectionnée. Defaults to None.

    Returns:
        list: Chaque élement correspond à une géographie (niveau supérieur, niveau transverse...)
    """    
    # Sélection des ionfos sur la zone qui seront affichées dans la page d'accueil
    ref = type_zones_base[type_zone]  # Conersion du type de zone en maille de la base de données
    conn = create_conn()
    cursor = conn.cursor()
    
    # 4 requêtes pour les 4 niveaux (inférieur, transverse, même maille que le document et supérieur)
    if ref !='France':
        queries = [
            f'''SELECT nom_doc
            FROM {os.environ.get('POSTGRE_SCHEMA')}.liste_doc_inf_sup ldis
            JOIN {os.environ.get('POSTGRE_SCHEMA')}.infos_docs_normalise idn
                ON ldis.code_doc = idn.code_doc
            WHERE ldis.type_geo = {"'"+ref+"'"}
                AND ldis.codegeo = {"'"+zone+"'"}
                AND ldis.niveau = 'contient';
            ''',
            f'''SELECT nom_doc
            FROM {os.environ.get('POSTGRE_SCHEMA')}.liste_doc_inf_sup ldis
            JOIN {os.environ.get('POSTGRE_SCHEMA')}.infos_docs_normalise idn
                ON ldis.code_doc = idn.code_doc
            WHERE ldis.type_geo = {"'"+ref+"'"}
                AND ldis.codegeo = {"'"+zone+"'"}
                AND ldis.niveau = 'égal';
            ''',
            f'''SELECT nom_doc
            FROM {os.environ.get('POSTGRE_SCHEMA')}.liste_doc_inf_sup ldis
            JOIN {os.environ.get('POSTGRE_SCHEMA')}.infos_docs_normalise idn
                ON ldis.code_doc = idn.code_doc
            WHERE ldis.type_geo = {"'"+ref+"'"}
                AND ldis.codegeo = {"'"+zone+"'"}
                AND ldis.niveau = 'transverse';
            ''',
            f'''SELECT  CONCAT(COUNT(ldis.code_doc),' ',ldis.type_doc)
            FROM {os.environ.get('POSTGRE_SCHEMA')}.liste_doc_inf_sup ldis
            JOIN {os.environ.get('POSTGRE_SCHEMA')}.infos_docs_normalise idn
                ON ldis.code_doc = idn.code_doc
            WHERE ldis.type_geo = {"'"+ref+"'"}
                AND ldis.codegeo = {"'"+zone+"'"}
                AND ldis.niveau = 'inclus'
                AND idn.etat_document in {str(etats_valides)}
            GROUP BY ldis.type_doc;
            '''
        ]

        formatted_infos = []

        for query in queries:  # AJout des résultats des 4 requêtes
            cursor.execute(query)
            infos = cursor.fetchall()
            formatted_info = ', '.join([info[0] for info in infos if info[0] is not None])  # Ajout des noms des documents à la suite
            formatted_infos.append(formatted_info)

        cursor.close()
        conn.close()

        return formatted_infos
    else:
        query = f'''
        SELECT CONCAT(COUNT(code_doc),' ',type_doc)
        FROM {os.environ.get('POSTGRE_SCHEMA')}.infos_docs_normalise
        WHERE infos_docs_normalise.etat_document in {str(etats_valides)}
        GROUP BY type_doc'''
        cursor.execute(query)
        infos = cursor.fetchall()
        formatted_info = ', '.join([info[0] for info in infos if info[0] is not None])
        cursor.close()
        conn.close()
        return ['','','',formatted_info]


def infos_sup_inf(type_zone, zone, docs):
    """Récupère les informations à afficher dans la fiche territoire

    Args:
        type_zone (str): Un des éléments de la liste type_zones
        zone (str): codegeo de la zone
        docs (list): liste des documents à prendre en compte

    Returns:
        list: un élément par tableau de la fiche territoire
    """    
    # Infos affichées dans la fiche territoire
    # prend en entrée un type de zone, une zone et la liste des type de documents pour lesquels on veut des infos
    ref = type_zones_base[type_zone]

    # Connexion à la base de données
    conn = create_conn()
    cursor = conn.cursor()

    # Nom des colonnes à afficher dans l'outil
    correspondance_colonnes = [
        'Nom du document',
        'Structure porteuse',
        "Etat d'avancement",
        "Sous état d'avancement",
        "Dernier évènement",
        'Population concernée',
        'Date de mise en application',
        'Date de fin de validité',
        'Liens'
    ]

    # Nom dees liens à afficher dans la case lien
    nom_liens = {
        'site_web': 'site du doc',
        'lien_centralise': 'fiche descriptive'
    }

    # Nom des colonnes à afficher pour les documents de niveau inférieur
    colonnes_niveau_inf = [
        'Nombre de documents',
        'Représentants une population de',
        'Et une superficie de'
    ]

    if ref != 'France':

        # Récupération de la population et de la superficie de la zone
        req_stats_territoire = f'''
        select pop, superf
        from {os.environ.get('POSTGRE_SCHEMA')}.liste_ref
        where codegeo = '{zone}' AND type_geo = '{ref}'
        '''
        cursor.execute(req_stats_territoire)
        stats_territoire = cursor.fetchall()
        stats_territoire = ['', stats_territoire[0][0], stats_territoire[0][1]]  # On doit garder la première colonne vide pour plus tard

        # Noms des docs pour lesquels on veut des infos
        req_docs = ''
        for doc in docs:  # On sélectionne uniquement les type de documents choisis par l'utilisateur
            req_docs += f" ldis.type_doc = '{doc}' OR"
        req_docs = req_docs[:-2]  # On enlève le OR à la fin de la requète sql
        
        req_docs_inf = ""
        for doc in types_docs:
            req_docs_inf += f" ldis.type_doc = '{doc}' OR"
        req_docs_inf = req_docs_inf[:-2]

        # Nom des colonnes à selctionner
        nom_colonnes = '''
            idn.nom_doc,
            idn.structure_porteuse,
            idn.etat_document,
            idn.sous_etat,
            CONCAT(idn.type_evt,' ',idn.date_evt) AS dernier_evenement,
            idn.pop,
            idn.date_creation,
            idn.date_expiration,
            idn.site_web,
            idn.lien_centralise,
            idn.liens_docs
        '''

        # Une requête par type géographique de document (égal à la zone, supérieur, transverse, inclus dans la zone)
        queries = [
            f'''SELECT {nom_colonnes}
            FROM {os.environ.get('POSTGRE_SCHEMA')}.liste_doc_inf_sup ldis
            JOIN {os.environ.get('POSTGRE_SCHEMA')}.infos_docs_normalise idn ON ldis.code_doc = idn.code_doc
            WHERE ldis.type_geo = {"'"+ref+"'"} AND ldis.codegeo = {"'"+zone+"'"} AND ldis.niveau = 'contient' AND (''' + req_docs + ''');
            ''',
            f'''SELECT {nom_colonnes}
            FROM {os.environ.get('POSTGRE_SCHEMA')}.liste_doc_inf_sup ldis
            JOIN {os.environ.get('POSTGRE_SCHEMA')}.infos_docs_normalise idn ON ldis.code_doc = idn.code_doc
            WHERE ldis.type_geo = {"'"+ref+"'"} AND ldis.codegeo = {"'"+zone+"'"} AND ldis.niveau = 'égal' AND (''' + req_docs + ''');
            ''',
            f'''SELECT {nom_colonnes}
            FROM {os.environ.get('POSTGRE_SCHEMA')}.liste_doc_inf_sup ldis
            JOIN {os.environ.get('POSTGRE_SCHEMA')}.infos_docs_normalise idn ON ldis.code_doc = idn.code_doc
            WHERE ldis.type_geo = {"'"+ref+"'"} AND ldis.codegeo = {"'"+zone+"'"} AND ldis.niveau = 'transverse' AND (''' + req_docs_inf + ''');
            ''',
            f'''
            WITH com_zone as (
                SELECT
                    codegeo
                    , pop
                    , superf
                FROM {os.environ.get('POSTGRE_SCHEMA')}.liste_ref
                WHERE
                    type_geo = 'commune'
                    AND {ref} = {"'"+zone+"'"}
                    OR ({ref}='commune' and codegeo = {"'"+zone+"'"})
            )

            , com_docs as (
                SELECT DISTINCT
                    ldis.type_doc
                    , dc.codegeo
                FROM {os.environ.get('POSTGRE_SCHEMA')}.liste_doc_inf_sup ldis
                JOIN {os.environ.get('POSTGRE_SCHEMA')}.infos_docs_normalise idn
                    ON ldis.code_doc = idn.code_doc
                LEFT JOIN {os.environ.get('POSTGRE_SCHEMA')}.docs_communes dc
                    on dc.code_doc = idn.code_doc
                WHERE
                    ldis.type_geo = {"'"+ref+"'"}
                    AND ldis.codegeo = {"'"+zone+"'"}
                    AND ({req_docs})
                    AND idn.etat_document in {str(etats_valides)}
                UNION
                SELECT DISTINCT
                    'PLUI ou PLU' as type_doc
                    , dc.codegeo
                FROM {os.environ.get('POSTGRE_SCHEMA')}.liste_doc_inf_sup ldis
                JOIN {os.environ.get('POSTGRE_SCHEMA')}.infos_docs_normalise idn
                    ON ldis.code_doc = idn.code_doc
                LEFT JOIN {os.environ.get('POSTGRE_SCHEMA')}.docs_communes dc
                    on dc.code_doc = idn.code_doc
                WHERE
                    ldis.type_geo = {"'"+ref+"'"}
                    AND ldis.codegeo = {"'"+zone+"'"}
                    AND (ldis.type_doc = 'PLU' or ldis.type_doc = 'PLUI' or ldis.type_doc = 'CC')
                    AND idn.etat_document in {str(etats_valides)}
                UNION
                SELECT DISTINCT
                    'DU' as type_doc
                    , dc.codegeo
                FROM {os.environ.get('POSTGRE_SCHEMA')}.liste_doc_inf_sup ldis
                JOIN {os.environ.get('POSTGRE_SCHEMA')}.infos_docs_normalise idn
                    ON ldis.code_doc = idn.code_doc
                LEFT JOIN {os.environ.get('POSTGRE_SCHEMA')}.docs_communes dc
                    on dc.code_doc = idn.code_doc
                WHERE
                    ldis.type_geo = {"'"+ref+"'"}
                    AND ldis.codegeo = {"'"+zone+"'"}
                    AND (ldis.type_doc = 'PLU' or ldis.type_doc = 'PLUI')
                    AND idn.etat_document in {str(etats_valides)}
            )

            , sup_pop AS (
                SELECT
                    com_docs.type_doc
                    , SUM(ref.pop::bigint) AS pop
                    , SUM(ref.superf::float) AS superf
                FROM com_docs
                JOIN com_zone ref
                    USING(codegeo)
                GROUP BY com_docs.type_doc
            )

            , nombre_docs AS (
                SELECT
                    CONCAT(COUNT(ldis.code_doc),' ',ldis.type_doc) AS Nombre_de_documents
                    , ldis.type_doc
                FROM {os.environ.get('POSTGRE_SCHEMA')}.liste_doc_inf_sup ldis
                JOIN {os.environ.get('POSTGRE_SCHEMA')}.infos_docs_normalise idn ON ldis.code_doc = idn.code_doc
                WHERE
                    ldis.type_geo = {"'"+ref+"'"}
                    AND ldis.codegeo = {"'"+zone+"'"}
                    AND ({req_docs})
                    AND idn.etat_document in {str(etats_valides)}
                GROUP BY ldis.type_doc
                UNION
                SELECT
                    CONCAT(COUNT(ldis.code_doc),' ','DU') AS Nombre_de_documents
                    , 'DU' as type_doc
                FROM {os.environ.get('POSTGRE_SCHEMA')}.liste_doc_inf_sup ldis
                JOIN {os.environ.get('POSTGRE_SCHEMA')}.infos_docs_normalise idn ON ldis.code_doc = idn.code_doc
                WHERE
                    ldis.type_geo = {"'"+ref+"'"}
                    AND ldis.codegeo = {"'"+zone+"'"}
                    AND (ldis.type_doc = 'PLU' or ldis.type_doc = 'PLUI' or ldis.type_doc = 'CC')
                    AND idn.etat_document in {str(etats_valides)}
                UNION
                SELECT
                    CONCAT(COUNT(ldis.code_doc),' ','PLUI ou PLU') AS Nombre_de_documents
                    , 'PLUI ou PLU' as type_doc
                FROM {os.environ.get('POSTGRE_SCHEMA')}.liste_doc_inf_sup ldis
                JOIN {os.environ.get('POSTGRE_SCHEMA')}.infos_docs_normalise idn ON ldis.code_doc = idn.code_doc
                WHERE
                    ldis.type_geo = {"'"+ref+"'"}
                    AND ldis.codegeo = {"'"+zone+"'"}
                    AND (ldis.type_doc = 'PLU' or ldis.type_doc = 'PLUI')
                    AND idn.etat_document in {str(etats_valides)}
            )

            SELECT
                nb.Nombre_de_documents
                , sp.pop
                , sp.superf
            FROM nombre_docs nb
            LEFT JOIN sup_pop sp
            USING(type_doc)
            '''
        ]
        t = []
        k = 0
        for query in queries:  # Pour chaque requête, on crée le tableau html correspondant
            # Récupération des infos
            if k == 3 and ref == 'commune':
                infos = []
            else:
                cursor.execute(query)
                infos = cursor.fetchall()

            if infos != []:
                table_html = '''
                <head>
                <script src="https://www.kryogenix.org/code/browser/sorttable/sorttable.js"></script>
                <style>
                #info_sup_inf {
                    font-family: Arial, Helvetica, sans-serif;
                    width: 100%;
                    overflow: auto;
                    max-height: 500px; /* Limitez la hauteur du conteneur */
                }

                #info_sup_inf table {
                    border-collapse: collapse;
                    width: 100%;
                }

                #info_sup_inf th, #info_sup_inf td {
                    border: 1px solid #ddd;
                    padding: 8px;
                    text-align: left;
                }

                #info_sup_inf th {
                    top: 0;
                    position: sticky;
                    background-color: #04AA6D;
                    color: white;
                }

                #info_sup_inf tr:nth-child(even) {
                    background-color: #f2f2f2;
                }

                #info_sup_inf tr:hover {
                    background-color: #ddd;
                }

                #info_sup_inf thead {
                    height: 20px;
                    width: 100%;
                }
                </style>
                </head>
                '''
                table_html += '''<body><div id="info_sup_inf"><table class="sortable">'''
                if k != 3:  # On différencie les documents de niveau inférieurs du reste
                    table_html += "<thead><tr>"
                    for nom in correspondance_colonnes:  # Noms des colonnes
                        table_html += f'''<th>{nom}</th>'''
                    table_html += "</tr></thead><tbody>"
                    
                    for row in infos:  # Parcourt des documents
                        table_html += '''<tr class="item">'''
                        i = 0
                        cell_liens = "<td>"
                        for cell in row:
                            nom = cursor.description[i].name
                            if nom == 'site_web' or nom == 'lien_centralise' or nom == 'liens_docs':  # si la case est un lien
                                if cell is not None:
                                    if nom != 'liens_docs':
                                        cell_liens += f"<a href='{cell}' target='_blank'>{nom_liens[nom]}</a>&nbsp;&nbsp"
                                    else:
                                        for lien in cell.split(';'):
                                            if lien != '':
                                                lien_sep = lien.split(' : ')
                                                cell_liens += f"<a href='{lien_sep[1]}' target='_blank'>{lien_sep[0]}</a>&nbsp;&nbsp"
                            elif cell is not None:
                                if i == 0:
                                    table_html += f"<td><b>{cell}</b></td>"
                                else:
                                    table_html += f"<td>{cell}</td>"
                            else:
                                table_html += "<td></td>"
                            i += 1
                        table_html += cell_liens
                        table_html += "</td>"
                        table_html += "</tr>"
                else:  # cas des documents de niveau inférieurs
                    pop_superf = ['', 'personnes', 'km²']  # unités
                    for nom in colonnes_niveau_inf:
                        table_html += f"<td><b>{nom}</b></td>"
                    table_html += "</tr>"
                    for row in infos:
                        table_html += "<tr>"
                        i = 0
                        for cell in row:
                            if cell is not None:
                                if i == 0:  # Nombre de documents opposables
                                    table_html += f"<td><b>{cell} en application</b></td>"
                                else:  # Calcul de la superficie du territoire couverte par tel ou tel document
                                    table_html += f"<td>{int(cell)} {pop_superf[i]} soit {int((cell)*100/stats_territoire[i])}% du territoire</td>"
                            else:
                                table_html += "<td></td>"
                            i += 1
                        table_html += "</tr>"
                table_html += "</tbody></table></div></body>"
                t.append(table_html)
            else:
                t.append('')
            k += 1
    else:  # maille = france entière (idem que dans l'autre cas sauf qu'il y a pas de where sur la zone)
        # Récupération de la population et de la superficie de la zone
        req_stats_territoire = f'''
        select sum(pop) as pop, sum(superf) as superf
        from {os.environ.get('POSTGRE_SCHEMA')}.liste_ref
        where type_geo = 'reg'
        '''
        req_docs_inf = ""
        for doc in types_docs:
            req_docs_inf += f" ldis.type_doc = '{doc}' OR"
        req_docs_inf = req_docs_inf[:-2]
        cursor.execute(req_stats_territoire)
        stats_territoire = cursor.fetchall()
        stats_territoire = ['', stats_territoire[0][0], stats_territoire[0][1]]  # On doit garder la première colonne vide pour plus tard

        req_docs = ''
        for doc in docs:
            req_docs += f" idn.type_doc = '{doc}' OR"
        req_docs = req_docs[:-2]
        query = f'''

            WITH com_zone as (
                SELECT
                    codegeo
                    , pop
                    , superf
                FROM {os.environ.get('POSTGRE_SCHEMA')}.liste_ref
                WHERE
                    type_geo = 'commune'
            )

            , com_docs as (
                SELECT DISTINCT
                    idn.type_doc
                    , dc.codegeo
                FROM {os.environ.get('POSTGRE_SCHEMA')}.infos_docs_normalise idn
                LEFT JOIN {os.environ.get('POSTGRE_SCHEMA')}.docs_communes dc
                    on dc.code_doc = idn.code_doc
                WHERE idn.etat_document in {str(etats_valides)}
                UNION
                SELECT DISTINCT
                    'DU' as type_doc
                    , dc.codegeo
                FROM {os.environ.get('POSTGRE_SCHEMA')}.infos_docs_normalise idn
                LEFT JOIN {os.environ.get('POSTGRE_SCHEMA')}.docs_communes dc
                    on dc.code_doc = idn.code_doc
                WHERE idn.etat_document in {str(etats_valides)}
                    AND (idn.type_doc = 'PLU' OR idn.type_doc = 'PLUI' OR idn.type_doc = 'CC')
                UNION
                SELECT DISTINCT
                    'PLUI ou PLU' as type_doc
                    , dc.codegeo
                FROM {os.environ.get('POSTGRE_SCHEMA')}.infos_docs_normalise idn
                LEFT JOIN {os.environ.get('POSTGRE_SCHEMA')}.docs_communes dc
                    on dc.code_doc = idn.code_doc
                WHERE idn.etat_document in {str(etats_valides)}
                    AND (idn.type_doc = 'PLU' OR idn.type_doc = 'PLUI')
            )

            , sup_pop AS (
                SELECT
                    com_docs.type_doc
                    , SUM(ref.pop::bigint) AS pop
                    , SUM(ref.superf::float) AS superf
                FROM com_docs
                JOIN com_zone ref
                    USING(codegeo)
                GROUP BY com_docs.type_doc
            )

            , nombre_docs AS (
                SELECT
                    CONCAT(COUNT(idn.code_doc),' ',idn.type_doc) AS Nombre_de_documents
                    , idn.type_doc
                FROM {os.environ.get('POSTGRE_SCHEMA')}.infos_docs_normalise idn
                WHERE idn.etat_document in {str(etats_valides)}
                GROUP BY idn.type_doc
                UNION
                SELECT
                    CONCAT(COUNT(idn.code_doc),' ', 'DU') AS Nombre_de_documents
                    , 'DU' as type_doc
                FROM {os.environ.get('POSTGRE_SCHEMA')}.infos_docs_normalise idn
                WHERE idn.etat_document in {str(etats_valides)}
                    AND (idn.type_doc = 'PLU' OR idn.type_doc = 'PLUI' OR idn.type_doc = 'CC')
                UNION
                SELECT
                    CONCAT(COUNT(idn.code_doc),' ', 'PLUI ou PLU') AS Nombre_de_documents
                    , 'PLUI ou PLU' as type_doc
                FROM {os.environ.get('POSTGRE_SCHEMA')}.infos_docs_normalise idn
                WHERE idn.etat_document in {str(etats_valides)}
                    AND (idn.type_doc = 'PLU' OR idn.type_doc = 'PLUI')
            )

            SELECT
                nb.Nombre_de_documents
                , sp.pop
                , sp.superf
            FROM nombre_docs nb
            LEFT JOIN sup_pop sp
            USING(type_doc);
            '''
        cursor.execute(query, (req_docs_inf,))
        infos = cursor.fetchall()
        data = pd.DataFrame(infos, columns=[desc[0] for desc in cursor.description])
        pop_superf = ['', 'personnes', 'km²']  # unités
        table_html = '''
        <head>
        <script src="https://www.kryogenix.org/code/browser/sorttable/sorttable.js"></script>
        <style>
        #info_sup_inf {
            font-family: Arial, Helvetica, sans-serif;
            width: 100%;
            overflow: auto;
            max-height: 500px; /* Limitez la hauteur du conteneur */
        }

        #info_sup_inf table {
            border-collapse: collapse;
            width: 100%;
        }

        #info_sup_inf th, #info_sup_inf td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }

        #info_sup_inf th {
            top: 0;
            position: sticky;
            background-color: #04AA6D;
            color: white;
        }

        #info_sup_inf tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        #info_sup_inf tr:hover {
            background-color: #ddd;
        }

        #info_sup_inf thead {
            height: 20px;
            width: 100%;
        }
        </style>
        </head>
        '''
        table_html += "<body><div id='info_sup_inf'><table class='sortable'>"
        table_html += "<thead><tr>"
        for nom in colonnes_niveau_inf:
            table_html += f"<th>{nom}</th>"
        table_html += "</tr></thead><tbody>"
        for row in infos:
            table_html += "<tr class='item'>"
            i = 0
            for cell in row:
                if cell is not None:
                    if i == 0:  # Nombre de documents opposables
                        table_html += f"<td><b>{cell} en application</b></td>"
                    else:  # Calcul de la superficie du territoire couverte par tel ou tel document
                        table_html += f"<td>{int(cell)} {pop_superf[i]} soit {int((cell)*100/stats_territoire[i])}% du territoire</td>"
                else:
                    table_html += "<td></td>"
                i += 1
            table_html += "</tr>"
        table_html += "</tbody></table></div></body>"
        t = ['', '', '', table_html]
    cursor.close()
    conn.close()
    return t


def select_docs():
    """cré la selectbox pour sélectionner le type de document en mode document

    Returns:
        None: le résultat est stocké dans st.session_state
    """    
    # Sélection du type de document en mode doc
    liste_docs = types_docs.copy()
    liste_docs.remove('PLU')
    liste_docs.remove('PLUI')
    liste_docs.remove('CC')
    if 'type_doc' not in st.session_state or st.session_state['type_doc'] == 'None':
        type_doc = st.sidebar.selectbox("Choisissez le type de document :", liste_docs + ['DU'])
        st.session_state['type_doc'] = type_doc
    else:
        types_docs_2 = pd.DataFrame(liste_docs + ['DU'])
        def index(df, valeur):
            return df[df == valeur].index[0]
        index = index(types_docs_2, st.session_state['type_doc'])
        type_doc = st.sidebar.selectbox("Choisissez le type de document :", types_docs_2, index)
        st.session_state['type_doc'] = type_doc


def choix_du_doc(type_doc, type_zone, zone):
    """Crée la selectbox pour choisir un document particulier en mode doc, le résultat est stocké dans st.session_state

    Args:
        type_doc (str): type de document
        type_zone (str): type de zone
        zone (str): codegeo de la zone géographique considérée
    """    
    # Choix du document en mode doc sous forme de st.selectbox
    if type_doc == 'DU':
        type_doc = ['PLU', 'PLUI', 'CC']
    else:
        type_doc = [type_doc]
    liste_type = f""
    for typ in type_doc:
        liste_type += f" type_doc = '{typ}' OR"
    liste_type = liste_type[:-2]
    conn = create_conn()
    cursor = conn.cursor()
    if type_zone == 'France entière':
        query = f'''
        SELECT code_doc, nom_doc FROM {os.environ.get('POSTGRE_SCHEMA')}.infos_docs_normalise
        WHERE {liste_type}'''
        cursor.execute(query)
        docs = pd.DataFrame(cursor.fetchall())
        docs.set_index(1,inplace=True)

    elif type_zone == 'Commune':
        liste_type_2 = liste_type.replace('type_doc', 'dc.type_doc')
        query = f'''
        SELECT dc.code_doc, nom_doc
        FROM {os.environ.get('POSTGRE_SCHEMA')}.docs_communes dc
        JOIN {os.environ.get('POSTGRE_SCHEMA')}.infos_docs_normalise USING(code_doc)
        WHERE dc.codegeo = '{zone}' AND ({liste_type_2})
        '''
        cursor.execute(query)
        docs = pd.DataFrame(cursor.fetchall())
        docs.set_index(1, inplace=True)

    else:
        liste_type_3 = liste_type.replace('type_doc', 'ldis.type_doc')
        query = f'''
        SELECT ldis.code_doc,nom_doc FROM {os.environ.get('POSTGRE_SCHEMA')}.liste_doc_inf_sup ldis
        JOIN {os.environ.get('POSTGRE_SCHEMA')}.infos_docs_normalise USING(code_doc)
        WHERE ({liste_type_3}) AND ldis.type_geo = {"'"+type_zones_base[type_zone]+"'"} AND ldis.codegeo = {"'"+zone+"'"}
        '''
        cursor.execute(query)
        docs = pd.DataFrame(cursor.fetchall())
        docs.set_index(1, inplace=True)

    cursor.close()
    conn.close()
    nom_doc = st.sidebar.selectbox("Choisissez un document :", docs.index)
    st.session_state['nom_doc'] = nom_doc
    st.session_state['code_doc'] = docs.loc[nom_doc].values[0]


# TODO : ajouter l'unité pour la superficie
# TODO : gérer toutes les colonnes de type option_nom_de_la_colonne
def infos_doc(code_doc, type_doc):
    """Crée le petit texte que s'affiche sous la carte en mode doc (et donne plus d'informations sur un document)

    Args:
        code_doc (str): code du codument
        type_doc (str): type de document

    Returns:
        str: Renvoie le résultat sous forme de str du html à afficher
    """    
    # affichage des informations sur un document en particulier
    if isinstance(code_doc, np.ndarray):  # empêche de créer une erreur s'il y a un doublon, a priori inutile si la base postgre est bien faite
        code_doc = code_doc[0]
    nom_table = f"{os.environ.get('POSTGRE_SCHEMA')}.infos_" + type_doc.lower()
    query = f'''
    SELECT * FROM {nom_table} WHERE code_doc= {"'"+code_doc+"'"}
    '''

    # Connexion à la base de données
    conn = create_conn()
    cursor = conn.cursor()

    cursor.execute(query)
    infos = cursor.fetchall()
    nom_liens = {  # case de lien
        'site_web': 'site du doc',
        'lien_centralise': 'fiche descriptive'
    }
    if infos:  # construit de la même manière que les autres tables html de l'app
        transposed_table = [[row[i] for row in infos] for i in range(len(infos[0]))]
        table_html = "<table>"
        i = 0
        d={}
        cell_liens = f"<tr><td><b>Liens : </b>"
        for row in transposed_table:
            colonne = cursor.description[i].name
            for cell in row:
                if colonne == 'site_web' or colonne == 'lien_centralise' or colonne == 'liens_docs':
                    if cell is not None:
                        if colonne != 'liens_docs':
                            cell_liens += f"<a href='{cell}' target='_blank'>{nom_liens[colonne]}</a>&nbsp;&nbsp"
                        else:
                            for lien in cell.split(';'):
                                if lien != '':
                                    t = lien.split(' : ')
                                    cell_liens += f"<a href='{t[1]}' target='_blank'>{t[0]}</a>&nbsp;&nbsp"
                elif cell is not None:
                    if colonne in correspondance_noms_docs.keys():
                        table_html += f"<tr><td><b>{correspondance_noms_docs[colonne]} :</b> {cell}</td></tr>"
                    else:
                        table_html += f"<tr><td><b>{colonne} :</b> {cell}</td></tr>" 
            i += 1
        table_html += cell_liens
        table_html += "</td></tr>"
        table_html += "</table>"
    else:
        table_html =  'SAGE abandonné'  # je ne sais pas d'où ça vient / à supprimer
    cursor.close()
    conn.close()
    return table_html


def checkbox_doc(doc):  # Checkbox pour choisir les docs affichés dans la fiche territoire
    """cré une checkbox pour savoir si un type de document doit être affiché ou non dans la fiche territoire

    Args:
        doc (str): type du doc
    """    
    if doc in st.session_state:
        if st.sidebar.checkbox(doc,value=st.session_state[doc]):
            st.session_state[doc] = True
        else:
            st.session_state[doc] = False
    else:
        if st.sidebar.checkbox(doc,True):
            st.session_state[doc] = True
        else:
            st.session_state[doc] = False


def choix_docs():  # récupère les infos que checbox_doc a mis dans session_state
    """Cré des checkbox pour savoir quels sont les documents à afficher en fiche territoire

    Returns:
        list(str): liste des types de documents à afficher dans la fiche territoire
    """    
    t = []
    for doc in types_docs:
        if st.session_state[doc] == True:
            t.append(doc)
    return t


def page_existe(url):
    """Fonction innutilisée actuellement

    Args:
        url (str): url

    Returns:
        bool: True si l'url existe, False sinon
    """    
    try:
        response = requests.get(url)
        # Si le code de statut de la réponse est 200, la page existe
        if response.status_code == 200:
            return True
        else:
            return False
    except requests.exceptions.RequestException:
        # Une exception RequestException peut être levée en cas d'erreur de réseau
        return False