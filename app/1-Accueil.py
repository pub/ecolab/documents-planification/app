#import des dufférents packages
import streamlit as st
st.set_page_config("Outil Documents de planification", layout='wide') #cette ligne doit être la première ligne de code streamlit exécutée (donc être avant import general)
import general as g
import mode_doc
import mode_territoire
from apscheduler.schedulers.background import BackgroundScheduler
from initialisation import initialisation_cache
from fiche_territoire import page_fiche_territoire


if 'type_zone' not in st.session_state:
    st.session_state['type_zone'] = 'France entière'
    st.session_state['type_zone_temp'] = 'France entière'


def main():  # fonction lançant la page d'accueil de l'app
    g.remove_page_items()  # retire des éléments interactifs de l'app natifs à streamlit (choix du mode d'affichage...)

    st.write("# Bienvenue sur l'outil de cartographie des documents de planification")  # texte de la page d'accueil
    text_presentation = '''
    ##### Cet outil est une version beta développée par l'Ecolab. N'hésitez pas à nous faire des retours sur [ce forms](https://docs.google.com/forms/d/e/1FAIpQLSffRpE7uF3RDVwKKmdLg7CfDa5UHwZjLHiCQq8YOi6uJUhJMQ/viewform?usp=sf_link) pour le faire évoluer.'''
    # ##### Remarque : pour analyser les documents, vous pouvez vous aider de la liseuse [liriae](https://liriae-form.lab.sspcloud.fr/)

    st.write(text_presentation)

    # Le scheduler lance la fonction initialisation_cache tous les jours à 4h UTC
    # Cette fonction permet de supprimer toutes les cartes et légende crées dans la journée pour les remplacer des données plus récentes
    # Aucune fonction streamlit ne doit être lancée par le Background scheduler
    scheduler = BackgroundScheduler() 
    scheduler.add_job(initialisation_cache, 'cron', day_of_week=0, hour=4, minute=0)
    scheduler.start()


    g.select_mode()  # fait apparaître les boutons permettant de choisir entre le mode territoire et le mode doc
    if st.session_state['boutons']['territoire'] and st.session_state['boutons'].get('fiche_territoire') is not True:  # lance le mode territoire si le bouton territoire a été cliqué
        mode_territoire.territoire()
    if st.session_state['boutons']['doc']:  # lance le mode doc si le bouton territoire a été cliqué
        mode_doc.doc()
    if st.session_state['boutons'].get('fiche_territoire') is True and st.session_state['boutons']['territoire']:
        page_fiche_territoire()


if __name__ == "__main__":
    main()
