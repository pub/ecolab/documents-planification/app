# Import des modules
import general as g
from map_functions import sous_etat_num, couleur_sous_etat, couleur_etat, etat_num
import map_functions as mf
import streamlit as st
from streamlit_extras.switch_page_button import switch_page
import os


def doc(): #affiche le mode doc
    if st.button('Cliquez ici pour voir la documentation'): #bouton pour voir la documentation
        switch_page('Documentation')

    st.sidebar.write("#### Sélection du type de document")
    g.select_docs()  # menu déroulant de sélection du document
    st.sidebar.write("#### Sélection du territoire à étudier")
    g.select_type_zone()  # menu déroulant de sélection du type de zone
    g.select_zone()  # menu déroulant de sélection de la zone


    ## Création de la carte
    if st.session_state['type_zone'] == 'France entière' or st.session_state['zone appelée'] == 1:
        # chemins où sont/seront stocké la carte/les légendes
        path = f"app/save_map/doc_{st.session_state['type_doc']}_{st.session_state['type_zone']}_{st.session_state['zone']}.html"
        legend_path1 = f"app/save_legend/legende1_{st.session_state['type_doc']}_{st.session_state['type_zone']}_{st.session_state['zone']}.html"
        legend_path2 = f"app/save_legend/legende2_{st.session_state['type_doc']}_{st.session_state['type_zone']}_{st.session_state['zone']}.html"
        try:
            if st.session_state['type_zone'] != 'France entière' or st.session_state['type_doc'] != 'DU':
                if os.path.isfile(path):  # affiche la carte si elle existe déjà
                    with open(path, 'r') as f:  # ouvre la carte
                        html_data = f.read()
                    st.components.v1.html(html_data, height=500)  # permet d'afficher du code html/CSS/JS dans streamlit
                    st.download_button(  # bouton pour télécharger la carte
                        data=html_data,
                        label='Télécharger la carte',
                        file_name=f"carte_{st.session_state['type_doc']}_{st.session_state['type_zone']}_{st.session_state['zone']}.html"
                    )
                else:  # crée la carte si elle n'existe pas
                    map, liste_sous_etats, liste_etats = mf.get_map_documents_3(st.session_state['type_doc'], st.session_state['type_zone'], st.session_state['zone'])
                    map.save(path)
                    with open(path, 'r') as f:  # ouvre la carte nouvellement créée
                        html_data = f.read()
                    st.components.v1.html(html_data, height=500)  # permet d'afficher du code html/CSS/JS dans streamlit
                    st.download_button(  # bouton pour télécharger la carte
                        data=html_data,
                        label='Télécharger la carte',
                        file_name=f"carte_{st.session_state['type_doc']}_{st.session_state['type_zone']}_{st.session_state['zone']}.html"
                    )

                    legende1 = ''  # création de la légende à partir des différents états d'avancement présents dans la carte
                    for etat in liste_etats:  # pour chaque état : affiche l'état et un carré de la couleur correspondante à partir du code html ci-dessous
                        legende1 += f'''
                        <span style="font-size: 15px;">
                            {etat} :  
                        </span>
                        <div class="box" style="display: inline-block; width: 15px; height: 15px; border: 7.5px solid {couleur_etat[etat_num[etat]]};margin-right: 10px;vertical-align: middle;"></div>
                        '''

                    legende2 = ''  # création de la légende à partir des différents sous-états d'avancement présents dans la carte
                    for sous_etat in liste_sous_etats:  # pour chaque sous-état : affiche l'état et un carré de la couleur correspondante à partir du code html ci-dessous
                        if sous_etat is not None:
                            legende2 += f'''
                            <span style="font-size: 15px;">
                                {sous_etat} :  
                            </span>
                            <div class="box" style="display: inline-block; width: 15px; height: 15px; border: 3px solid {couleur_sous_etat[sous_etat_num[sous_etat]]};margin-right: 10px;vertical-align: middle;"></div>
                            '''
                    with open(legend_path1, 'w') as f:  # stocke les fichier correspondant à la légende
                        f.write(legende1)
                    with open(legend_path2, 'w') as f:
                        f.write(legende2)

                # Affichage de la légende à partir des fichiers html correspondants
                st.write('### Légende :')
                with open(legend_path1, 'r') as f: 
                    legende_html1 = f.read()
                with open(legend_path2, 'r') as f:
                    legende_html2 = f.read()
                st.write(legende_html1, unsafe_allow_html=True)
                st.write(legende_html2, unsafe_allow_html=True)
            else:
                st.write("La carte ne veut pas s'afficher, essayez une maille inférieure")
        except:
            st.error('''Une erreur a eu lieu, veuillez réessayer votre action.
            Si l'erreur persiste, merci de la signaler sur ce [formulaire](https://docs.google.com/forms/d/e/1FAIpQLSffRpE7uF3RDVwKKmdLg7CfDa5UHwZjLHiCQq8YOi6uJUhJMQ/viewform?usp=sf_link)
            ''')

        # Affichage d'informations sur le document choisi
        
        try:
            st.write('### Informations suplémentaires :')
            st.sidebar.write("#### Sélection d'un document particulier")
            g.choix_du_doc(st.session_state['type_doc'], st.session_state['type_zone'], st.session_state['zone'])  # choix du document
            st.write(g.infos_doc(st.session_state['code_doc'], st.session_state['type_doc']), unsafe_allow_html = True)  # affichage des informations
        except:
            st.write('#### Pas de documents dans cette zone')

