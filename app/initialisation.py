#Fichier d'initialisation de l'outil (suppression des cartes et légende déjà créées et créations de nouvelles )
# Seules les cartes les plus longues à charger sont créées ici, les autres seront chargées lorsque des utilisateurs voudront les afficher.
import os
import streamlit as st
import map_functions as mf
import general as g
from map_functions import couleur_etat, couleur_sous_etat, etat_num, sous_etat_num

def stockage_doc(type_doc, type_zone, zone):  # Création d'une carte du mode doc
    if type_zone != 'France entière' or type_doc != 'DU':  # La carte avec tous les DU fait crasher le site
        path = f"app/save_map/doc_{type_doc}_{type_zone}_{zone}.html"  # Chemin de la carte
        legend_path1 = f"app/save_legend/legende1_{type_doc}_{type_zone}_{zone}.html"  # Chemin de la legende (partie 1)
        legend_path2 = f"app/save_legend/legende2_{type_doc}_{type_zone}_{zone}.html"  # Chemin de la legende (partie 2)
        map, liste_sous_etats, liste_etats = mf.get_map_documents_3(type_doc, type_zone, zone)  # Création de la carte
        map.save(path)  # Enregistrement de la carte

        legende1 = ''  # Création de la légende (partie 1)
        for etat in liste_etats:
            legende1 += f'''
            <span style="font-size: 15px;">
                {etat} :  
            </span>
            <div class="box" style="display: inline-block; width: 15px; height: 15px; border: 7.5px solid {couleur_etat[etat_num[etat]]};margin-right: 10px;vertical-align: middle;"></div>
            '''

        legende2 = ''  # Création de la légende (partie 1)
        for sous_etat in liste_sous_etats:
            if sous_etat is not None:
                legende2 += f'''
                <span style="font-size: 15px;">
                    {sous_etat} :  
                </span>
                <div class="box" style="display: inline-block; width: 15px; height: 15px; border: 3px solid {couleur_sous_etat[sous_etat_num[sous_etat]]};margin-right: 10px;vertical-align: middle;"></div>
                '''
        with open(legend_path1, 'w') as f:  # Enregistrement de la légende (partie 1)
            f.write(legende1)
        with open(legend_path2, 'w') as f:  # Enregistrement de la légende (partie 2)
            f.write(legende2)


def stockage_territoire(maille, zone, type_zone):  # Création d'une carte du mode territoire
    path = f"app/save_map/territoire_{maille}_{zone}_{type_zone}.html"  # Chemin de la carte
    map = mf.get_map_test_popup_2(maille, zone, type_zone)
    map.save(path)


def initialisation_cache():  # supression des cartes existante et création des nouvelles
    # Spécifiez le chemin du répertoire que vous souhaitez vider.
    save_legend = "app/save_legend"  # répertoire contenant les légendes
    save_map = "app/save_map"  # répertoire contenant les cartes

    # Obtenez la liste de tous les fichiers et dossiers dans le répertoire cible.
    contenu_dossier1 = os.listdir(save_legend)
    # Parcourez la liste et supprimez chaque fichier.
    for element in contenu_dossier1:
        element_path = os.path.join(save_legend, element)
        if os.path.isfile(element_path):
            os.remove(element_path)

    contenu_dossier2 = os.listdir(save_map)
    # Parcourez la liste et supprimez chaque fichier.
    for element in contenu_dossier2:
        element_path = os.path.join(save_map, element)
        if os.path.isfile(element_path):
            os.remove(element_path)
    
    regions = g.liste_zones('Région').values  # liste des régions

    # Chargement des cartes du mode doc
    for document in g.types_docs:  # liste des documents
        stockage_doc(document, 'France entière', None)  # Stockage de la carte du doc pour la zone 'France entière'
        for x in regions:
            region = x[0].split(" ")[0]  # conversion de 'numéro_région Région' à 'numéro_région'
            stockage_doc(document, 'Région', region)  # Stockage de la carte du doc pour la région

    # Chargement des cartes du mode territoire
    for x in regions: #liste des régions
        region = x[0].split(" ")[0]  # conversion de 'numéro_région Région' à 'numéro_région'
        stockage_territoire('Commune', region, 'Région')  # Stockage de la carte de la région pour la maille communale
        stockage_territoire('EPCI', region, 'Région')  # Stockage de la carte de la région pour la maille epci
    # idem avec les départements
    departements = g.liste_zones('Département').values
    for x in departements:
        dep = x[0].split(" ")[0]
        stockage_territoire('Commune', dep, 'Département')
        stockage_territoire('EPCI', dep, 'Département')
    print('operation terminée')
