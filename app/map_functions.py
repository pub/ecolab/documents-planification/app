__doc__ = "Fonctions pour produire une carte des installations"

import folium
import pandas as pd
import geopandas as gpd
from general import connexion_str, engine, type_zones_base, create_conn
import os
import json
from folium.plugins import Fullscreen

# TODO : changer les couleurs par des codes hexadécimaux plus jolis
couleur_etat = ['white', 'orange', 'black', 'green', 'black', 'white', 'blue', 'yellow', 'black']  # Correspondance entre la couleur d'affichage sur la carte et le numéro de l'état d'avancement
couleur_sous_etat = ['black', 'purple', 'red', 'darkblue', 'pink', 'purple', 'darkblue', 'gray']  # idem sous état d'avancement
zoom = {'France entière': 5, 'Région': 7, 'Département': 9, 'EPCI': 10, 'Commune': 11}  # Correspondance maille-zoom
etat_num = {  # Correspondance état d'avancement - numéro de l'état d'avancement
    'Etat inconnu': 0,
    'En élaboration': 1,
    'Non démarré': 2,
    'Opposable': 3,
    'Annulé': 4,
    'Remplacé': 5,
    'Approuvé': 6,
    'Partiellement annulé': 7,
    'Abandonné': 8,
    'Signé': 3,
    'Adopté': 3
}
sous_etat_num = {  # idem sous-état d'avancement
    "Elaboration - Mise en place": 1,
    "Elaboration -  Participation du public": 2,
    "Elaboration - Rédaction": 3,
    "Mise en œuvre hors révision ou modification": 4,
    'Pas de protocole': 5,
    "Protocole d'accord signé": 2,
    "Protocole d'accord non signé": 6,
    "Révision": 7,
    None: 0,
    'CC': 1,
    'PLU': 3,
    'PLUi': 2,
    'PLUI': 2,
    'Obligé': 2,
    'Volontaire': 3,
    "PLUi-D volontaire": 1,
    "PGD ou autre": 2,
    "PDU volontaire": 3,
    "PDM volontaire": 4,
    "PDM obligatoire": 5,
    "PLUi-D obligatoire": 6,
    "PDU obligatoire": 7
}


# Chargement des contours de la carte (mode territoire)
def load_contours_2(maille, zone, type_zone):  # TODO : changer la fonction pour utiliser psycopg2
    """Récupère tous les polygones d'une zone donnée pour une maille donnée

    Args:
        maille (str): maille
        zone (str): codegeo de la zone
        type_zone (str): type de zone

    Returns:
        geoDataFrame: renvoie un dataframe avec les coordonnée et le codegeo de tous les polygones
    """    
    if type_zone == 'Commune':  # requete SQL à effectuer si la zone sélectionnée est une commune
        # type_zones_base permet de convertir le nom de la maille dans l'outil vers le nom de la maille dans SQL
        requete = f'''
            SELECT
                geom
                , codegeo
                , nom
            FROM {os.environ.get('POSTGRE_SCHEMA')}.liste_ref b
            WHERE b.type_geo={"'" + type_zones_base[maille] + "' "}
            AND b.codegeo = {"'" + zone + "';"}
        '''
    elif type_zone != 'France entière':  # Requête à effectuer si la zone est 'France entière'
        # type_zones_base permet de convertir le nom de la maille dans l'outil vers le nom de la maille dans SQL
        requete = f'''
        SELECT
            geom
            , codegeo
            , nom
            FROM {os.environ.get('POSTGRE_SCHEMA')}.liste_ref b
            WHERE b.type_geo={"'" + type_zones_base[maille] + "' "}
            AND b.{type_zones_base[type_zone]} = {"'" + zone + "';"}
        '''
    else:  # Requête à effectuer si l'on est pas dans un des deux cas précedent
        # type_zones_base permet de convertir le nom de la maille dans l'outil vers le nom de la maille dans SQL
        requete = f'''
        SELECT 
            geom
            , codegeo
            , nom
            FROM {os.environ.get('POSTGRE_SCHEMA')}.liste_ref b
            WHERE b.type_geo={"'" + type_zones_base[maille] + "';"}
        '''
    return gpd.GeoDataFrame.from_postgis(requete, connexion_str, geom_col='geom')  # Conversion de la requête SQL en geodataframe


def localisation_zoom(zone, type_zone):  # TODO : Changer le nom de la fonction (elle ne donne pas le zoom), changer pour psycopg2
    """renvoie la localisation d'une zone sur la carte

    Args:
        zone (str): codegeo de la zone
        type_zone (str): type de la zone

    Returns:
        list: liste latitude, longitude
    """    
    # prend en entrée la zone et le type de zone
    # renvoie la localisation de la zone
    if type_zone == 'France entière':
        return [46.227638, 2.213749]  # Coordonnées du centre de la france

    # Requête SQL donnant les coordonnées 
    # type_zones_base permet de convertir le nom de la maille dans l'outil vers le nom de la maille dans SQL
    requete = f'''
        SELECT
            latitude
            , longitude
        FROM {os.environ.get('POSTGRE_SCHEMA')}.liste_ref
        WHERE codegeo ={"'"+ zone + "'"}
        AND type_geo= {"'" + type_zones_base[type_zone] + "';"}
    '''
    return pd.read_sql(requete, engine).values.tolist()[0]  # le [0] sert à ne prendre que la première ligne de la requête sql (même s'il n'y a qu'une ligne)


def get_map_test_popup_2(maille, zone, type_zone):  # Crée la carte du mode territoire
    """Crée la carte du mode territoire

    Args:
        maille (str): maille d'affichage
        zone (str): codegeo de la zone à afficher
        type_zone (str): type de la zone

    Returns:
        folium map: carte folium du mode territoire
    """    
    # Prend en entrée la maille, la zone et le type de zone
    # Renvoie la carte correspondance sous forme de carte folium

    ref = type_zones_base[maille]  # ref = la maille telle qu'elle s'appelle dans la base postgresql
    map = folium.Map(location=localisation_zoom(zone, type_zone), zoom_start=zoom[type_zone])  # Création de la carte
    contours = load_contours_2(maille, zone, type_zone)  # Chargement des polygones de la carte
    group = folium.FeatureGroup(zone)  # Pas forcément utile, à voir si on l'enlève

    # Établir une connexion à la base de données PostgreSQL
    conn = create_conn()
    cursor = conn.cursor()

    for row in contours.iterfeatures():  # parcourt des différents polygones
        codegeo = row['properties']['codegeo']  # récupération du codegeo
        libgeo = row['properties']['nom']  # récupération du libgeo
        geo = folium.GeoJson(row, style_function=lambda feature: {  # Conversion du polygone en couche folium
            'fillColor': '#8fb3a4',
            'color': 'black',
            'weight': 0.5,
            'fillOpacity': 0.8,
        }, name=codegeo, tooltip=codegeo + ' ' + libgeo)

        # Récupéreration de tous les documents autre que ceux de niveau infèrieur pour le polygone sélectionné
        query = f'''
        SELECT DISTINCT idn.nom_doc
            , idn.structure_porteuse
            , idn.etat_document
            , idn.sous_etat
            , CONCAT(idn.type_evt,' : ',idn.date_evt) as dernier_evenement
            , idn.site_web
            , idn.lien_centralise
            , idn.liens_docs
        FROM {os.environ.get('POSTGRE_SCHEMA')}.liste_doc_inf_sup ldis
        JOIN {os.environ.get('POSTGRE_SCHEMA')}.infos_docs_normalise idn ON idn.code_doc = ldis.code_doc
        WHERE ldis.codegeo = {"'"+codegeo+"'"} and (niveau = 'égal' OR niveau = 'contient' OR niveau = 'transverse') AND ldis.type_geo = {"'"+ref+"'"}'''
        cursor.execute(query)  # exécution de la requête
        infos = cursor.fetchall()  # récupération du résultat de la requête
        colonnes = [  # Noms de colonnes à afficher dans l'outil, les noms doivent être dans le même ordre que dans la requête
            'Nom du document',
            'Structure porteuse',
            "Etat d'avancement",
            "Sous état d'avancement",
            "Dernier évènement",
            'Liens'
        ]
        nom_liens = {  # comment les url cliquables doivent s'afficher dans les popups
            'site_web': 'site du doc',
            'lien_centralise': 'fiche descriptive'
        }
        if infos:  # si la requête à renvoyé quelque chose :
            # TODO : Rendre la tableau interactif (tri des colonnes en cliquant dessus)
            table_html = '''
            <head>
            <style>
            #info_sup_inf {
                font-family: Arial, Helvetica, sans-serif;
                width:700px;
                overflow: auto;
                max-height: 450px; /* Limitez la hauteur du conteneur */
            }

            #info_sup_inf table {
                border-collapse: collapse;
                width: 100%;
            }

            #info_sup_inf th, #info_sup_inf td {
                border: 1px solid #ddd;
                padding: 8px;
                text-align: left;
            }

            #info_sup_inf th {
                top: 0;
                position: sticky;
                background-color: #04AA6D;
                color: white;
            }

            #info_sup_inf tr:nth-child(even) {
                background-color: #f2f2f2;
            }

            #info_sup_inf tr:hover {
                background-color: #ddd;
            }

            #info_sup_inf thead {
                height: 20px;
                width: 100%;
            }
            </style>
            </head>
            '''
            table_html += '''<div id="info_sup_inf"><table class="sortable">'''
            table_html += "<thead><tr>"  # Ajout de la première ligne (ligne des colonnes)
            for nom in colonnes:  # Parcourt des colonnes
                table_html += f"<th style='border: 1px solid black;'><b>{nom}</b></th>"  # Ajout du nom de la colonne sur la première ligne
            table_html += '</tr></thead><tbody>'  # Fin de la première ligne
            for row in infos:  # Parcourt des documents renvoyés par la requête sql
                table_html += "<tr class='item'>"  # Début de la ligne
                i = 0  # Numéro de la colonne
                cell_liens = "<td style='border: 1px solid black;'>"  # Création de la case contenant les url
                for cell in row:  # Parcourt des cases de la ligne sélctionnée
                    nom = cursor.description[i].name  #Nom de la colonne de la case
                    if nom == 'site_web' or nom == 'lien_centralise' or nom == 'liens_docs':  # Si la colonne correspond à un url, on ajoute ça dans la case url
                        if cell is not None:  # On vérifie que la case n'est pas vide
                            if nom != 'liens_docs':
                                cell_liens += f"<a href='{cell}' target='_blank'>{nom_liens[nom]}</a>&nbsp;&nbsp"  # On ajoute le lien à la case des liens
                            else:  # La colonne 'liens_docs peut contenir plusieurs liens on sépare donc chacun des liens avant de les ajouter à la case des liens
                                for lien in cell.split(';'):  # Séparation des liens
                                    if lien != '':
                                        t = lien.split(' : ')  # Séparation du nom et de l'url du lien
                                        cell_liens += f"<a href='{t[1]}' target='_blank'>{t[0]}</a>&nbsp;&nbsp"  # On ajoute le lien suivi d'espaces à la case du lien
                    elif cell is not None:  # cas ou la case n'est pas un lien
                        if i == 0:  # Si c'est la première colonne, on affiche la case en gras
                            table_html += f"<td style='border: 1px solid black;'><b>{cell}</b></td>"
                        else:  # Sinon on affiche la case (pas en gras)
                            table_html += f"<td style='border: 1px solid black;'>{cell}</td>"
                    else:  # Si la case est vide, on cré une case vide
                        table_html += "<td style='border: 1px solid black;'></td>"
                    i += 1
                table_html += cell_liens  # On ajoute la case des liens à la ligne
                table_html += "</td>"  # On ferme la case des liens
                table_html += "</tr>"  # On change de ligne
            table_html += "</tbody></table></div>"  # On ferme la table
            folium.Popup(table_html).add_to(geo)  # On ajoute le tableau créé en popup du contour correspondant
        geo.add_to(group)  # On ajoute la couche (le contour/polygone/codegeo)  au featureGroup

    # Fermer la connexion à la base de données
    cursor.close()
    conn.close()

    group.add_to(map)  # Ajout du groupe à la carte
    Fullscreen().add_to(map)  # Ajout du mode plein écran
    folium.TileLayer('cartodbpositron').add_to(map)  # Ajout du fond de carte
    return map


def get_map_documents_3(type_de_doc, type_zone, zone):  # Crée la carte du mode doc
    """Crée la carte du mode doc

    Args:
        type_de_doc (str): type de document
        type_zone (str): type de zone
        zone (str): codegeo de la zone

    Returns:
        map (folium map): carte du mode doc
        liste_sous_etats (list): liste des sous états d'avancement présents sur la carte
        liste_etats (list): liste des sous états d'avancement présents sur la carte
    """    
    # Prend en entrée un type de document, un type de zone, et une zone
    # Renvoie la carte correspondante, la liste des états d'avancement présents dans la carte, la liste des sous états d'avancement présents dans la carte
    liste_sous_etats = []  # Initialisation de la liste des sous_etats
    liste_etats = []  # Initialisation de la liste des etats

    # Création de la connexion à la base de données
    
    map = folium.Map(  # Initialisation de la carte
        location=localisation_zoom(zone, type_zone)
        , zoom_start=zoom[type_zone]
    )
    Fullscreen().add_to(map)  # Ajout du plein écran
    folium.TileLayer('cartodbpositron').add_to(map)  # ajout du fond de carte
    def group_layer(type_doc):
        conn = create_conn()
        cursor = conn.cursor()
        if type_zone != 'France entière':  # requête SQL pour obtenir tous les documents de France
            query = f'''
            SELECT code_doc 
            FROM {os.environ.get('POSTGRE_SCHEMA')}.liste_doc_inf_sup
            WHERE type_doc = {"'"+type_doc+"'"} AND type_geo = {"'" + type_zones_base[type_zone] + "'"} AND codegeo = {"'" + zone + "'"} 
            '''
            cursor.execute(query)
            docs = cursor.fetchall()
        else:  # requête SQL pour obtenir tous les documents d'une zone particulière
            query = f'''
            SELECT code_doc FROM {os.environ.get('POSTGRE_SCHEMA')}.infos_docs_normalise
            WHERE type_doc = {"'"+type_doc+"'"}'''
            cursor.execute(query)
            docs = cursor.fetchall()

        def nom_des_colonnes(type_doc):  # Initialisation du nom des colonnes à sélectionné (toutes les colonnes sauf la première)
            requete = f'''
                SELECT column_name 
                FROM information_schema.columns 
                WHERE table_schema = '{os.environ.get('POSTGRE_SCHEMA')}' AND table_name = 'infos_docs_normalise' ;
            '''
            cursor.execute(requete)
            result = cursor.fetchall()
            return ','.join(row[0] for row in result[1:])

        group = folium.FeatureGroup(name=type_doc, control=True)  # Création du FeatureGroup (à supprimer éventuellement)
        colonnes = [  # Noms des colonnes dans l'outil
            'Nom du document',
            'Structure porteuse',
            "Etat d'avancement",
            "Sous état d'avancement",
            "Dernier évènement",
            'Liens'
        ]
        nom_liens = {  # Correspondance entre les noms des colonnes de lien dans postgre et les noms des liens dans l'outil
            'site_web': 'site du doc',
            'lien_centralise': 'fiche descriptive'
        }
        for l_doc in docs:  # Parcourt de tous les documents obtenu par la requête sql
            doc = l_doc[0]  #récupération du code_doc

            # requête pour récupérzer les contours du document
            query_1 = f'''
            SELECT geom
            FROM {os.environ.get('POSTGRE_SCHEMA')}.contours_doc
            WHERE code_doc = {"'"+doc+"'"}
            '''

            # requête pour récupérer les infos sur le document
            query_2 = f'''
            SELECT nom_doc
                , structure_porteuse
                , etat_document
                , sous_etat
                , CONCAT(type_evt,' : ',date_evt) as dernier_evenement
                , site_web
                , lien_centralise
                , liens_docs
            FROM  {os.environ.get('POSTGRE_SCHEMA')}.infos_docs_normalise
            WHERE code_doc = {"'"+doc+"'"}
            '''

            # Exécution des requ^tes
            cursor.execute(query_1)
            geom = cursor.fetchall()
            cursor.execute(query_2)
            infos = cursor.fetchall()

            if geom != []:  # On vérifie que les contours du document sont disponibles
                if infos:  # On vérifie que les informations sur le document sont disponibles
                    table_html = "<table>"  # création du tableau (la popup)
                    table_html += "<tr>"  # Création de la première ligne
                    for nom in colonnes:  # Ajout des noms des colonnes sur la première ligne de la popup
                        table_html += f"<td style='border: 1px solid black;'><b>{nom}</b></td>"
                    table_html += "</tr>"
                    for row in infos:  # parcourt des lignes de la requête donnnant les informations sur le document (il y a une seule ligne en réalité)
                        table_html += "<tr>"  # création de la ligne
                        i = 0  # initialisation du numéro de la colonne
                        cell_liens = "<td style='border: 1px solid black;'>"  # création de la case des liens
                        for cell in row:  # parcourt des cases de la ligne
                            nom = cursor.description[i].name  # récupération du nom de la colonne
                            if nom == 'site_web' or nom == 'lien_centralise' or nom == 'liens_docs':  # Si la colonne est une colonne de lien, on ajoute les infos à la case lien
                                if cell is not None:
                                    if nom == 'site_web':  # On ajoute le lien à la case lien
                                        cell_liens += f"<a href='{cell}' target='_blank'>{nom_liens[nom]}</a>&nbsp;&nbsp"
                                    elif nom == 'lien_centralise':
                                        for lien in cell.split(';'):
                                            if lien != '':
                                                cell_liens += f"<a href='{lien}' target='_blank'>{nom_liens[nom]}</a>&nbsp;&nbsp"
                                    else:  # si la colonne est 'lien_docs, on sépare les liens présents dans la case avant de les ajouter à la case de liens
                                        for lien in cell.split(';'):
                                            if lien != '':
                                                t = lien.split(' : ')
                                                cell_liens += f"<a href='{t[1]}' target='_blank'>{t[0]}</a>&nbsp;&nbsp"
                            elif cell is not None:  # Cas où la colonne n'est pas un lien
                                if i == 0:  # on met la première colonne en gras
                                    table_html += f"<td style='border: 1px solid black;'><b>{cell}</b></td>"
                                    nom_du_document = cell  # On enregistre le nom du document
                                else:  # Si ce n'est pas la première colonne, on crée simplement la case
                                    table_html += f"<td style='border: 1px solid black;'>{cell}</td>"
                                if cursor.description[i].name == 'etat_document':  # On enregistre l'état d'avancement
                                    etat_1 = cell
                                elif cursor.description[i].name == 'sous_etat':  # On enregistre le sous-état d'avancement
                                    sous_etat_1 = cell
                            else:
                                table_html += "<td style='border: 1px solid black;'></td>"  # s'il n'y a pas d'info, on crée une case vide
                            i += 1
                        table_html += cell_liens  # on ajoute la case des liens
                        table_html += "</td>"  # on ferme la case des liens
                        table_html += "</tr>"  # on ferme la dernière ligne
                    table_html += "</table>"  # on ferme le tableau
                    try:  # On ajoute le sous-état à la liste des sous-états s'il n'est pas déjà dedans
                        if sous_etat_1 not in liste_sous_etats:
                            liste_sous_etats.append(sous_etat_1)
                    except:  # ne dois pas arriver
                        sous_etat_1 = None
                    try:  # On ajoute l'état à la liste des états s'il n'est pas déjà dedans
                        if etat_1 not in liste_etats:
                            liste_etats.append(etat_1)
                    except:  # ne dois pas arriver
                        _ = None

                    # si l'état est défini, le sous-état aussi et qu'ils font parti de la liste des états possible alors on affiche le contour en conséquence
                    try:
                        etat_2 = etat_num[etat_1]
                        if sous_etat_1 in sous_etat_num.keys():
                            sous_etat_2 = sous_etat_num[sous_etat_1]
                        else:
                            sous_etat_2 = 0
                        style = {
                        'fillColor': couleur_etat[etat_2],
                        'color': couleur_sous_etat[sous_etat_2],
                        'weight': 0.5,
                        'fillOpacity': 0.8,}
                    # Sinon on affiche tout en noir
                    except:
                        style = {
                        'fillColor': 'black',
                        'color': 'black',
                        'weight': 0.9,
                        'fillOpacity': 0.8,}

                    # Il faut créer cette fonction suivi de cette row pour que folium accepte de créer le polygone avec les couleurs définies précédemment
                    def style_f(feature):
                        return feature['properties']['style']
                    row = {'id': '0', 'type': 'Feature', 'properties': {'style': style}, 'geometry': json.loads(geom[0][0])}

                    carte_doc = folium.GeoJson(row, style_function=style_f, tooltip=nom_du_document)  # Création de la couche folium
                    folium.Popup(table_html).add_to(carte_doc)  # Ajout de la popup à la couche folium
                    carte_doc.add_to(group)  # Ajout au FeatureGroup
        cursor.close()
        conn.close()
        return group

    if type_de_doc != 'DU':
        group = group_layer(type_de_doc)
        group.add_to(map)
    else:
        for doc in ['PLU', 'PLUI', 'CC']:
            group = group_layer(doc)
            group.add_to(map)   # Ajout du FeatureGroup à la carte
            folium.Marker(location=(0, 0)).add_to(group)
        folium.LayerControl().add_to(map)
    return map, liste_sous_etats, liste_etats
